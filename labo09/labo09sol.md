# Laboratoire 9 : Processus

## 1. Processus et jobs (30 minutes)

- Démarrez un terminal et entrez la commande `ps`. Existe-t-il des liens de
  parenté entre les processus qui sont affichés?

<details>
	<summary>Solution : </summary>

```sh
$ ps
  PID TTY          TIME CMD
 3019 pts/0    00:00:00 bash
17630 pts/0    00:00:00 ps
```
Nous supposons que le processus `ps` est un processus enfant de `bash`.
</details><br>

- Confirmez votre réponse précédente en entrant `ps -F`.

<details>
	<summary>Solution : </summary>

```sh
$ ps -F
UID        PID  PPID  C    SZ   RSS PSR STIME TTY          TIME CMD
jean      3019  3015  0  5719  5460   3 19:56 pts/0    00:00:00 bash
jean     25651  3019  0  9342  3356   1 20:17 pts/0    00:00:00 ps -F
```
Le `PPID` (l'ID du processus parent) de `ps -F` est le même que le `PID` de
`bash`.
</details><br>

- Lancez les processus `xeyes` et `gnome-calculator` en arrière-plan en une
  seule commande.

<details>
	<summary>Solution : </summary>

```sh
$ xeyes & gnome-calculator &
```
</details><br>

- Comparez les résultats des commandes `jobs`, `jobs -r` et `jobs -s`.

<details>
	<summary>Solution : </summary>

`jobs` affiche toutes les tâches, `jobs -r` affiche les tâches en cours
d'exécution	(non suspendues), `jobs -s` affiche celles suspendues ou
stoppées.
Ici, les deux tâches de la sous-question précédente sont en cours
d'exécution :
```sh
# après avoir lancé `xeyes & gnome-calculator &`
$ jobs
[1]-  En cours d'exécution   xeyes &
[2]+  En cours d'exécution   gnome-calculator &
$ jobs -r
[1]-  En cours d'exécution   xeyes &
[2]+  En cours d'exécution   gnome-calculator &
$ jobs -s
$
```
</details><br>

- Ramenez l'application `xeyes` en premier plan à l'aide de la commande `fg`.

<details>
	<summary>Solution : </summary>

Le numéro de la tâche pour `xeyes` est `1` (résultat de `jobs`)
```sh
$ fg 1
xeyes
```
</details><br>

- Interrompez `xeyes` avec `CTRL-Z`.

<details>
	<summary>Solution : </summary>

```sh
$ fg 1
xeyes
^Z
[1]+  Stoppé                 xeyes
```
</details><br>

- Vérifiez l'état des jobs avec `jobs`.

<details>
	<summary>Solution : </summary>

```sh
$ jobs
[1]+  Stoppé                 xeyes
[2]-  En cours d'exécution   gnome-calculator &
```
</details><br>

- À l'aide de la commande `kill`, terminez l'application `xeyes`.

<details>
	<summary>Solution : </summary>

```sh
$ kill %1
[1]-  Terminated              xeyes
```
</details><br>

- Vérifiez l'état des jobs avec `jobs`.

<details>
	<summary>Solution : </summary>

```sh
$ jobs
[2]+  En cours d'exécution   gnome-calculator &
```
</details><br>

- Terminez l'application `xeyes`.

<details>
	<summary>Solution : </summary>

```sh
$ kill %2
[2]+  Terminated              gnome-calculator
```
</details><br>

- Vérifiez l'état des jobs avec `jobs`.

<details>
	<summary>Solution : </summary>

```sh
$ jobs
$
```
</details><br>

- Relancez à nouveau une instance de `xeyes` et de `gnome-calculator` en
  arrière-plan.

<details>
	<summary>Solution : </summary>

```sh
$ xeyes & gnome-calculator &
```
</details><br>

- Ramenez `xeyes` en avant-plan avec `fg`, puis interrompez-la avec `CTRL-Z`.

<details>
	<summary>Solution : </summary>

```sh
$ jobs
[1]-  En cours d'exécution   xeyes &
[2]+  En cours d'exécution   gnome-calculator &
$ fg 1
xeyes
^Z
[1]+  Stoppé                 xeyes
```
</details><br>

- Entrez la commande `ps -f` et notez la valeur PID de `xeyes`.

<details>
	<summary>Solution : </summary>

```sh
$ ps -f
UID        PID  PPID  C STIME TTY          TIME CMD
jean      3019  3015  0 19:56 pts/0    00:00:00 bash
jean     11415  3019  0 21:03 pts/0    00:00:00 xeyes
jean     11416  3019  0 21:03 pts/0    00:00:00 gnome-calculator
jean     14514  3019  0 21:06 pts/0    00:00:00 ps -f
```
Dans mon exemple, le PID de `xeyes` est `11415`.
</details><br>

- Lancez la commande `kill PID`, où `PID` est la valeur correspondante à
  `xeyes`.

<details>
	<summary>Solution : </summary>

```sh
$ kill 11415
```
</details><br>

- Est-ce que le processus a été interrompu? Pourquoi?

<details>
	<summary>Solution : </summary>
Non, en lançant `ps -f`, on voit que le processus `xeyes` n'a pas été
interrompu. Car, par défaut, `kill` envoie le signal `SIGTERM` demandant au
processus de se terminer. Comme il est stoppé, il ne peut pas recevoir de
signal, ce dernier sera mis en attente.
</details><br>

- Ramenez `xeyes` en avant-plan avec `fg`.

<details>
	<summary>Solution : </summary>

```sh
$ fg 1
xeyes
Terminated
```
</details><br>

- Est-ce que `xeyes` est maintenant interrompu? Pourquoi?

<details>
	<summary>Solution : </summary>

Oui, on peut également le vérifier avec `ps -f`. En ramenant en premier
plan le processus `xeyes`, celui-ci s'est remis en exécution et a reçu le
signal (en attente) de terminaison.
</details><br>

## 2. Signaux (10 minutes)

**Note** : Cet exercice s'apparente plus à une expérience à faire par vous-même.

- Lancez la commande `ls -R /`.  
- Interrompez-la avec `CTRL-C`.  
- Ensuite, lancez la même commande en arrière-plan `ls -R / &`.
- Que se passe-t-il si vous essayez de l'interrompre avec `CTRL-C`?

<details>
	<summary>Solution : </summary>
Il ne se passe rien, car `Ctrl-C` ne fonctionne (n'envoie le signal SIGINT)
que sur les tâches en avant-plan.
</details><br>

- Entrez `fg` suivi de `Enter`, pour remettre la commande en avant-plan, puis
  interrompez-la avec `CTRL-C`.


## 3. Terminer de force (20 minutes)

- Récupérez le script `love` disponible dans le répertoire courant.
- Rendez-le exécutable `chmod +x love`
- Lancez la commande `./love` normalement (en avant-plan).
- Interrompez-la avec `CTRL-C`.
	
<details>
		<summary>Remarque : </summary>
Le script ne s'arrête pas, mais à chaque `CTRL-C`, le script affiche une 
ligne d'une chanson.
</details><br>

- Forcez la terminaison du processus:

1. avec `kill` à partir d'un autre terminal,

<details>
	<summary>Solution : </summary>

Si nous tentions avec :
```sh
$ pgrep love
13685
$ kill 13685
```
Ou avec
```sh
$ pkill love
```
Le processus n'est pas terminé. Il faut donc forcer la terminaison avec :
```sh
$ pgrep love
13685
$ kill --signal SIGKILL 13685
```
Ou avec
```sh
$ pkill --signal SIGKILL love
```
</details><br>

2. en fermant le terminal,
3. avec `kill` à partir du terminal courant.

<details>
	<summary>Solution : </summary>

Le script `love` est exécuté en avant-plan, donc on peut le suspendre, puis
forcer la terminaison :
```sh
$ ./love
^C
What is love?
^C
Baby, don't hurt me
^Z
[1]+  Stoppé                 ./love
 $ kill -SIGKILL %1
[1]+  Killed                  ./love
```
</details><br>

- Assurez-vous que le processus est bien terminé avec `ps -eF | grep love` (ou
  `pgrep -f love`)

<details>
	<summary>Solution : </summary>

Le résultat des deux commandes nous montrent qu'il n'y a plus de processus
`love` actif.
</details><br>

## 4. Récursivité (20 minutes)

- Récupérez le script `fib` disponible dans le répertoire courant.
- Rendez-le exécutable `chmod +x fib`
- Lancez la commande `./fib 5` en arrière-plan et notez son `PID`.
- Lancez la commande `pstree PID`, où `PID` est l'identifiant du script lancé
  juste avant. Combien y a-t-il de processus `sleep` au total? Combien de
  processus `fib`?

<details>
	<summary>Solution : </summary>

```sh
$ ./fib 5 &
[1] 31487
$ pstree 31487
fib─┬─fib─┬─fib─┬─fib─┬─2*[fib───sleep]
    │     │     │     └─sleep
    │     │     ├─fib───sleep
    │     │     └─sleep
    │     ├─fib─┬─2*[fib───sleep]
    │     │     └─sleep
    │     └─sleep
    ├─fib─┬─fib─┬─2*[fib───sleep]
    │     │     └─sleep
    │     ├─fib───sleep
    │     └─sleep
    └─sleep
$ pgrep -c fib
15
$ pgrep -c sleep
15
```
</details><br>

- Terminez tous les processus fib avec `pkill` et `killall`

<details>
	<summary>Solution : </summary>

```sh
$ pkill fib
[1]+  Terminated              ./fib 5
```

Ou

```sh
$ killall fib
[1]+  Terminated              ./fib 5
```

</details><br>

- Lancez les commandes `./fib 0`, `./fib 1`, ..., `./fib 7` en arrière-plan.
- Combien y a-t-il de processus `sleep` dans chaque cas? De processus `fib`?

<details>
	<summary>Solution : </summary>

Pour `n = 1, 2, ..., 7`, on lance les commandes `./fib n &` puis `pgrep -c fib`
et `pgrep -c sleep`, par exemple pour `n = 7` :

```sh
$ ./fib 7 &
[1] 31279
$ pgrep -c fib
41
$ pgrep -c sleep
41
```

Voici un récapitulatif des résultats pour chaque `n` :

| `./fib n &` | `pgrep -c fib` | `pgrep -c sleep` |
| ----------- | :------------: | :--------------: |
| `./fib 0 &` | 1              | 1                |
| `./fib 1 &` | 1              | 1                |
| `./fib 2 &` | 3              | 3                |
| `./fib 3 &` | 5              | 5                |
| `./fib 4 &` | 9              | 9                |
| `./fib 5 &` | 15             | 15               |
| `./fib 6 &` | 25             | 25               |
| `./fib 7 &` | 41             | 41               |

</details><br>

- Proposez des formules qui indiquent le nombre de processus `sleep` et `fib`
  si on appelle la commande `./fib <n>`, où `<n>` est un nombre entier positif.

<details>
	<summary>Solution : </summary>

Le script `fib` contient des appels [récursifs](https://fr.wikipedia.org/wiki/Algorithme_r%C3%A9cursif)
vers lui-même. Par exemple, lorsqu'on exécute `./fib <n>`, le script exécute 
`fib <n-1>` et `fib <n-2>`, créant des processus enfants qui exécuterons à leur
tour d'autres scripts. 

Soit `nbFib(n)` le nombre de processus `fib` créer à l'étape `n` dans 
`./fib <n>`, alors pour $`n > 1`$ :

$`nbFib(n) = 1 + nbFib(n-1) + nbFib(n-2)`$

Ceci nous fait penser à la [suite de Fibonacci](https://fr.wikipedia.org/wiki/Suite_de_Fibonacci)
définie par :

$`f_0 = 1`$<br>
$`f_1 = 1`$<br>
$`f_n = f_{n-1} + f_{n-2},\ \text{pour } n \ge 2`$<br>

Remarquons ce qui suit :

| `n` | `nbFib(n)` | $`f_n`$ |
| :-: | :--------: | :-----: |
| 0   | 1          | 1       |
| 1   | 1          | 1       |
| 2   | 3          | 2       |
| 3   | 5          | 3       |
| 4   | 9          | 5       |
| 5   | 15         | 8       |
| 6   | 25         | 13      |
| 7   | 41         | 21      |

Nous avons que $`nbFib(n) = 2f_n - 1`$, cette formule peut se prouver par 
induction sur `n`.

</details><br>

- Pourquoi `fib` n'apparait pas dans les premiers résultats de `top` ?

## 5. Ressources (20 minutes)

**Note** : Cet exercice s'apparente plus à une expérience à faire par vous-même.

- Avec l'interface graphique, lancez chrome (ou chromium) avec plusieurs
  onglets
- Avec l'interface graphique, lancez libreoffice (ou openoffice) et ouvrez
  <http://www.openoffice.org/documentation/whitepapers/Creating_large_documents_with_OOo.odt>
- Dans une console. trouvez les processus associés à ces outils avec `ps` et
  avec `top`
- Combien de mémoire et de CPU utilisent-ils?
- Visitez <https://browserbench.org/MotionMark1.1/> et regardez en temps réel
  `top` 1. l'utilisation CPU et 2. la charge moyenne
