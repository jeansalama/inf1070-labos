# Laboratoire 7 - révisions

Beaucoup de questions, concentrez-vous sur les questions qui vous semblent nécessaires.

## 1. Questions en vrac (30 minutes)

* Quel est la taille du plus gros fichier de `/usr/lib` en octets ? puis en mégaoctets ? (utilisez `find`, `sort`, `tail` et `numfmt`)
* Créez un fichier régulier `toto` de 1234567 octets, avec 2 liens durs, modifié le 20 janvier 1998, avec les droits `r---w---x`. (utilisez `head`, `ln`, `/dev/zero`, `touch`, `chmod`)
* Trouvez 2 variations différentes de `chmod` pour changer les droits de `toto` vers `rw-rw-r--`.
* Compressez `toto` avez `gzip`. Quel est le gain? Pouvez-vous trouver un moyen de compresser plus?
* Créez un fichier vide qui s'appelle « `l'* ou l'☆` », renommez-le en « `-|-` » puis supprimez-le. Refaites cet exercice en utilisant différentes approches: échappement avec « `\` », « `"` » et « `'` »; utilisation de globs; utilisation de la complètement (touche de tabulation); ainsi qu'utilisation de `find -exec`.
* Créez un fichier `f0`, un répertoire `d1` qui contient un fichier `f1` et un répertoire `d2` qui contient un fichier `f2`. Placez-vous dans `d2` avec la commande `cd` et créez dans `d1` trois liens symbolique: `l0`, `l1` et `l2` qui pointent respectivement vers les fichiers `f0`, `f1` et `f2` (vérifiez que vos liens symboliques fonctionnent bien).
* Trouvez les mots du dictionnaire français qui possèdent un unique « `è` » mais aucun autre « `e` » (avec ou sans accents).
* Trouvez des paires de mots de la GPL (`/usr/share/common-licenses/GPL`) qui sont séparés par un espace tels que le premier commence par « `ex` » et le second a deux lettres et commence par « `o` ». Par exemple « `exclusively on` ».
* Quelles lignes de la GPL ne sont pas vide et ne contiennent pas de « `e` » (ni en majuscule, ni en minuscule). (l'option `-e` de `grep` permet d'essayer plusieurs expressions régulières).

## 2. Questions de réflexion (30 minutes)

Le répertoire courant contient

```sh
$ ls -l
total 40
-rw-r--r-- 1 privat privat  220 oct 23 09:46 Z10.txt
-rw-r--r-- 1 privat privat 1045 oct 23 09:46 Z1.txt
-rw-r--r-- 1 privat privat  632 oct 23 09:46 Z2.txt
-rw-r--r-- 1 privat privat  587 oct 23 09:46 Z3.txt
-rw-r--r-- 1 privat privat  266 oct 23 09:46 Z4.txt
-rw-r--r-- 1 privat privat  222 oct 23 09:46 Z5.txt
-rw-r--r-- 1 privat privat  318 oct 23 09:46 Z6.txt
-rw-r--r-- 1 privat privat  450 oct 23 09:46 Z7.txt
-rw-r--r-- 1 privat privat   78 oct 23 09:46 Z8.txt
-rw-r--r-- 1 privat privat  159 oct 23 09:46 Z9.txt
```

Soit la commande suivante:

```sh
cat *.txt | tr -cd '[:alnum:]' | grep -o '[0-9]*' > list
```

* Que fait cette commande? 
* Combien de programmes sont exécutées? 
* Qu'est-ce qu'est responsable de faire l'expansion des noms de fichiers du glob «`*.txt`» ? 
* Qu'est-ce qu'est responsable de faire l'analyse de l'expression régulière «`[0-9]*`» ?
* Est-ce que dans cette commande `tr` utilise des expressions régulières?
* Combien et quels arguments reçoivent (en vrai) les programmes exécutés ?
* Peut on remplacer « `cat *.txt` » par « `< *.txt` » ?
* Que se passe-t-il si on remplace « `> list` » par « `2> list` » ? Même question avec « `>> list` » ?
* Que se passe-t-il si on enlève le tiret « `-` »  du `tr -cd` ? Si on le remplace par un double tiret « `--` »? Si on le remplace par un tube « `|` » ? 
* La politique interne de l'entreprise interdit l'utilisation de la commande `tr` les mois d'octobre. Remplacez le `tr` par un `sed`.

## 3. Correction du TP1 (30 minutes)

Correction rapide du TP1 en lab.


## 4. Questions de cours (30 minutes)

* Citez 3 distributions Linux.
* Citez deux domaines de l'informatique ou les systèmes basés sur UNIX (y compris Linux) sont majoritaires (>50%).
* Quelles sont les sections habituelles des pages de man?
* Citez deux formats d'images. Quels sont leurs avantages respectifs ?
* J'ai fait « `sudo rm -rf /usr`» . Qu'est-ce que j'ai perdu ? Comment réparer mon erreur ?
* Même question pour « `rm -rf ~/*` »
* J'ai fait « `cat lolcat.gif` ». Pourquoi j'ai de la bouillie dans mon terminal au lieu d'une photo drôlatique de chat? Comment puis-je voir ma photo de chat? 
* Je télécharge une vidéo de chatons mignons de 30Mio à 10kb/s. Dans combien de minutes j'aurais fini le téléchargement (en supposant que la vitesse de téléchargement reste constante). 

