# Laboratoire 11 - Réseau

## 1. Adresses IP (10 minutes)

- En ligne de commande, récupérez votre adresse IP privée à l'aide de la
  commande `ip`. Prenez-la en note.

```sh
$ ip -brief address show | grep UP
```

- Ensuite, à l'aide de la commande `dig`, récupérez votre adresse IP publique.


```sh
$ dig +short myip.opendns.com @resolver1.opendns.com
```

## 2. *Socket* (30 minutes)

### 2.1. Bases de netcat

- Dans un terminal, entrez la commande `nc -l 3333`.
- Ouvrez un fureteur et rendez-vous à `localhost:3333`.
- Que voyez-vous apparaître dans le terminal?

Solution : l'entête de la requête GET du fureteur vers le terminal.

- Rafraîchissez la page du fureteur. Est-ce que le terminal affiche de
  nouvelles informations? Pourquoi?

Solution : non, le terminal a fermé la connexion après la première requête.

- Refaites le même exercice en ajoutant l'option `-k` à la commande `nc`.
  Au besoin, consultez le manuel (`man nc`).

```sh
$ nc -kl 3333 
```

### 2.2. Communications réseau

En équipe de deux étudiants, transmettez vous des messages par l'intermédiaire
de la commande netcat:

- Récupérez d'abord vos adresses IP respectives.

```sh
$ ip -brief address show | grep UP
```

- Puis un premier étudiant ouvre une socket en mode « écoute » sur un port
  spécifique (par exemple 3333).

```sh
$ nc -l 3333
```

- Le deuxième étudiant envoie un message à l'aide de
  `echo message | nc adresse_ip`.

```sh
$ echo "allo" | nc <adresse-ip-recuperee> 3333
```

- Inversez ensuite les rôles.

## 3. Téléchargement (30 minutes)

### 3.1. Interrompre et relancer un téléchargement

- À l'aide de la commande `curl`, lancez le téléchargement de la ressource
  https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.18.9.tar.xz

```sh
$ curl -O https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.18.9.tar.xz
```

- Interrompez-le avec `Ctrl-C`
- Puis relancez le téléchargement à l'aide de l'option `-C`.

```sh
$ curl -C - -O https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.18.9.tar.xz
```

### 3.2. Charger les locaux d'un cours

Sur le site de l'UQAM, il est possible d'afficher les locaux d'un cours en se
rendant sur la page https://etudier.uqam.ca. Par exemple, pour visualiser
l'horaire du cours INF1070, il suffit d'aller à l'adresse
https://etudier.uqam.ca/cours?sigle=inf1070.

Écrivez un petit script nommé `cours` qui prend en paramètre le sigle d'un
cours de l'UQAM (par exemple `inf1070`) et qui affiche sur la sortie standard
les locaux dans lesquels le cours se déroule.

Par exemple, on s'attend à ce que la commande

```sh
./cours inf1070
```

affiche le résultat

```sh
SH-R810
SH-3420
SH-3420
```

```sh
#! /bin/bash

curl -s "https://etudier.uqam.ca/cours?sigle=$1" | grep -oE '[A-Z]{2}-[R0-9]{4}'
```

## 4. SSH et Tmux (40 minutes)

### 4.1. Connexion aux serveurs de Labunix

- À l'aide de la commande `ssh`, connectez-vous à la machine
  `java.labunix.uqam.ca`.

```sh
$ ssh <ton-code-MS>@java.labunix.uqam.ca
Password:<ton-mot-de-passe-MS>
```

- Quels sont les autres utilisateurs connecté, et de quelle machines
  viennent-ils ? Utilisez la commande `who`.

```sh
$ who
```

- Explorez sommairement le système de fichiers disponible dans votre répertoire
  personnel.

Solution : utiliser `ls` et `cd`

- Quel est le résultat de `ls /home/`? Croyez-vous qu'il soit sécuritaire
  d'avoir accès à cette information?

Solution : on voit les répertoires personnels des autres utilisateurs. 

- À l'aide de la commande `cd`, essayez de visiter le répertoire d'un autre
  utilisateur. Que se passe-t-il?

Solution: Nous n'avons pas la permission de visiter le répertoire personnel d'un
autre utilisateur. On n'a accès uniquement qu'au nôtre.

- Consultez les permissions des répertoires contenus dans `/home`.

```sh
$ ls -l /home/
```

- Déconnectez-vous du serveur `java`.

```sh
$ exit
déconnexion
Connection to java.labunix.uqam.ca closed.
```

- Connectez-vous maintenant à la machine `zeta.labunix.uqam.ca`. Est-ce que les
  mêmes fichiers sont disponibles dans votre répertoire personnel? Pourquoi?


### 4.2. Génération de clés

- Générez une paire de clé RSA privée/publique en lançant la commande
  interactive `ssh-keygen` sans argument et en suivant les instructions.

```sh
$ ssh-keygen
```

- Affichez le contenu de la clé publique sur la sortie standard à l'aide de la
  commande `cat`. **Note**: Il n'est pas toujours sécuritaire d'afficher la clé
  privée, car elle peut parfois être enregistrée dans l'historique de session
  de terminal.

```sh
$ ls ~/.ssh
id_rsa  id_rsa.pub  known_hosts
$ cat ~/.ssh/id_rsa.pub
```
 
- À l'aide de la commande `ssh-copy-id`, transférez votre paire de clés sur le
  serveur `java`.

```sh
$ ssh-copy-id <code-MS>@java.labunix.uqam.ca
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
Password:<mot-de-passe-MS>

```
 
- Confirmez qu'il est maintenant possible de vous connecter sur `java` sans
  devoir saisir votre mot de passe.

```sh
$ ssh <code-MS>@java.labunix.uqam.ca
```
 

### 4.3. Tmux

En consultant les dernières diapositives du chapitre 7 (réseau),
familiarisez-vous avec le logiciel Tmux:

- Connectez-vous en SSH sur le serveur `java`.
- Puis lancez une nouvelle session avec `tmux`.
- Créez plusieurs panneaux avec `Ctrl-B %` et `Ctrl-B "`.
- Changez la disposition des panneaux avec `Ctrl-B o` et `Espace`.
- Affichez l'heure avec `Ctrl-B t`.
- Détachez-vous de la session avec `Ctrl-B d`.
- Créez une deuxième session en relançant `tmux` puis quittez-la avec
  `Ctrl-B d`.
- Entrez la commande `tmux ls` pour lister les sessions existantes. Repérez les
  deux sessions que vous venez de créer.
- Chargez la première session que vous aviez créée avec `tmux a -t <nom>` où
  `<nom>` est remplacé par le nom de la session. Renommez-la session courante
  avec un nom plus facile à retenir et vérifiez le changement avec `Ctrl-B s`.
- Lancez le script [`ecrit`](./ecrit) disponible dans ce dépôt, détachez-vous
  de la session et déconnectez-vous du serveur `java`.
- Reconnectez-vous sur Java et chargez à nouveau la session `tmux` dans
  laquelle vous aviez lancé le script. Vérifiez que le script a bien continué
  de tourner malgré votre déconnection.
