
# Interface système

L'intention de ce cours exercice est de démontrer la proximité de l'interface
système dans Vim.

## Énoncé

### Commande `:read`

Considérant le fichier `recette-gateau.md` duquel on trouve l'extrait suivant :

```
- Placer le chocolat dans un bol.
- Dans une petite casserole, porter à ébullition la crème et le sirop de maïs. Verser sur le chocolat. Laisser reposer 2 minutes avant de remuer.
- Mélanger au fouet jusqu’à ce que la ganache soit lisse et homogène. Réfrigérer 1 heure ou jusqu’à ce qu’elle ait épaissi et qu’elle soit manipulable, en la remuant à quatre ou cinq reprises.
```

On observe que les listes à puces ne sont pas convenables pour structurer les
étapes d'une recette puisque la préparation se fait en étapes ordonnées. Il faut
vraisemblablement utiliser une liste numérotée. Voir le résultat désiré pour un
des paragraphes ci-après :

```
1. Placer le chocolat dans un bol.
2. Dans une petite casserole, porter à ébullition la crème et le sirop de maïs. Verser sur le chocolat. Laisser reposer 2 minutes avant de remuer.
3. Mélanger au fouet jusqu’à ce que la ganache soit lisse et homogène. Réfrigérer 1 heure ou jusqu’à ce qu’elle ait épaissi et qu’elle soit manipulable, en la remuant à quatre ou cinq reprises.
```

On souhaite donc changer toutes les listes à puce par des listes numérotées.

- **Concept utile** : insertion en bloc (voir `:help v_b_I`)
- **Commande Vim** : `:read!` (voir `:help :read!`)
- **Touches Vim** : `d`, `ctrl-v`, `p`
- **Programmes utiles** : `seq` (voir `man seq`)

### Commande `:write`

Vous travaillez sur un algorithme (fichier `algo.c`) depuis un bout de temps et
avez besoin de conseil d'un ami qui s'y connait bien. Celui-ci n'a
malheureusement pas son ordinateur avec lui et ne peux pas cloner votre dépôt
`git` pour le moment, mais il a accès à un navigateur sur son téléphone mobile.
Il est prêt à vous donner un conseil si vous lui partagez votre code sur une
page web facile d'accès.

Vous devez donc envoyer un fichier sur un *Pastebin*. Utilisez la commande
suivante à partir de Vim directement afin de partager votre code avec votre
ami :

```sh
curl -F 'sprunge=<-' http://sprunge.us
```

Vous pouvez aussi partager une partie du fichier seulement en utilisant le mode
visuel avant de lancer la commande à l'interface système.

**Commande Vim** : `:write` (voir `:help :write`)

### Math avec Perl

On souhaite évaluer une expression mathématique comme `3+4*2/3+5`. Avec le
terminal, exécutez :

```sh
$ perl -e '$x = 3+4*2/3+5; print $x'
```

Utilisez la configuration de touche suivante afin de faire un calcul d'une
expressions sous le curseur :

```vim
:vnoremap <C-m> "aygvmaomb<ESC>:r !perl -e '$x = <C-R>a; print $x'<CR>"ay$dd`bv`a"ap$
```

Ensuite, surlignez (avec `v`) une expression comme la suivante et appuyez
sur `ctrl-m`.

```
3+4*101+5/303*4000
```

Pour garder cette configuration, ajoutez la ligne `vmap` dans votre fichier
`~/.vimrc` en omettant le préfixe `:`.

<!-- vim: set sts=4 ts=4 sw=4 tw=80 et :-->

