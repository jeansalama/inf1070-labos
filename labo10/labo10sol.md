# Laboratoire 10 - scripts shell

## Redirections et substitutions (30 minutes)

* Écrivez un script shell POSIX `ave` qui affiche « `Bonjour!` » sur la sortie standard et « `Au revoir!` » sur la sortie d'erreur standard

```sh
#! /bin/sh

echo "Bonjour!"
echo "Au revoir!" >&2
```

* Soit la commande shell « `./ave >b 2>a` »; que contiennent les fichiers `a` et `b` ?
* Qu'affiche la commande shell « `{ ./ave | rev ; } 2>&1 | rev` » ?
* Modifiez le script `ave` pour qu'il affiche le nom de l'utilisateur en plus en utilisant la variable d'environnement `USER`. Par exemple « `Bonjour privat!` » et « `Au revoir privat!` »

```sh
#! /bin/sh

echo "Bonjour $USER!"
echo "Au revoir $USER!" >&2
```

* Qu'affiche maintenant la commande « `USER=Annonyme ./ave` » ?
* Modifiez le script `ave` pour utiliser la substitution de la commande « `id` » à la place de `USER`. Regardez les option de `id` pour avoir juste le nom de l'utilisateur.

```sh
#! /bin/sh

echo "Bonjour $(id -un)!"
echo "Au revoir $(id -un)!" >&2
```

* Qu'affiche maintenant la commande « `USER=Annonyme ./ave` » ?

## Évaluation des enseignements (interlude)

* Faites l'évaluation en ligne des enseignements http://www.evaluation.uqam.ca/

## Script shell (60 minutes)

Dans les exercices qui suivent, referez-vous à la section **structures de contrôles** du [chapitre 6: scripts shell](http://info.uqam.ca/~privat/INF1070/06b-script.pdf)

### if et tests

Faites un script shell POSIX `foobar` qui prend un seul argument et l'affiche à l'écran. Si l'argument est le nombre 3, le script affiche `foo` à la place.
Si l'argument est le nombre 4, le script affiche `bar` à la place.
Si l'argument est le nombre 12, le script affiche `foobar` à la place.

Exemple:

```
$ ./foobar toto
toto
$ ./foobar 30
30
$ ./foobar 3
foo
$ ./foobar 4
bar
$ ./foobar 12
foobar
$ ./foobar foo
foo
```

* Utilisez `if` et `[` (ou `test`)

```sh
#! /bin/sh

if [ "$1" = "3" ]; then
    echo "foo"
elif [ "$1" = "4" ]; then
    echo "bar"
elif [ "$1" = "12" ]; then
    echo "foobar"
else
    echo "$1"
fi
```

### Filtrage

Modifiez `foobar` pour que si l'argument n'est pas un nombre entier positif, le programme affiche un message d'erreur sur la sortie standard d'erreur et se termine avec un code de retour de 1.

Exemple:

```
$ ./foobar 12
foobar
$ echo $?
0
$ ./foobar toto
foobar: toto n'est pas un entier
$ echo $?
1
```

* Vous pouvez tester si l'argument est un nombre entier avec `grep` et une expression régulière.

```sh
#! /bin/bash

if grep -vqE -e "^[0-9]+$" <<<"$1" ; then
    echo "foobar: $1 n'est pas un entier" >&2
    exit 1
elif [ $1 -eq 3 ]; then
    echo "foo"
elif [ $1 -eq 4 ]; then
    echo "bar"
elif [ $1 -eq 12 ]; then
    echo "foobar"
else
    echo "$1"
fi
```

Cependant l'énoncé nous demande de faire un script shell POSIX, or `<<<` est un
bashisme. Donc, si on avait `#! /bin/sh` comme première ligne, on aurait une
erreur avec `<<<`

```sh
$ ./foobar 12
./foobar: 3: ./foobar: Syntax error: redirection unexpected
```

Notre script devient

```sh
#! /bin/sh

if echo "$1" | grep -vqE -e "^[0-9]+$" ; then
    echo "foobar: $1 n'est pas un entier" >&2
    exit 1
elif [ $1 -eq 3 ]; then
    echo "foo"    
elif [ $1 -eq 4 ]; then
    echo "bar"
elif [ $1 -eq 12 ]; then
    echo "foobar"
else
    echo "$1"
fi
```

Ou encore sans `grep`

```sh
#! /bin/sh

[ $1 -gt 0 ] 2> /dev/null

if [ $? -ne 0  ]; then
    echo "foobar: $1 n'est pas un entier" >&2
    exit 1
elif [ $1 -eq 3 ]; then
    echo "foo"    
elif [ $1 -eq 4 ]; then
    echo "bar"
elif [ $1 -eq 12 ]; then
    echo "foobar"
else
    echo "$1"
fi
```


### Développement arithmétique

Modifiez `foobar` pour que le programme affiche `foo` pour tous les multiples de 3 (et plus seulement pour le nombre 3 lui-même), `bar` pour les multiples de 4 et `foobar` pour les multiples de 12.

Exemple:

```
$ ./foobar 50
50
$ ./foobar 3
foo
$ ./foobar 30
foo
$ ./foobar 8
bar
$ ./foobar 240
foobar
```

* Utilisez le développement arithmétique `$(())`
* Note: l'opérateur `%` (modulo) retourne le reste de la division entière. « x%y » vaut 0 si x est un multiple de y.

```sh
#! /bin/sh

if echo "$1" | grep -vqE -e "^[0-9]+$" ; then
    echo "foobar: $1 n'est pas un entier" >&2
    exit 1
elif [ $(($1 % 12)) -eq 0 ]; then
    echo "foobar"
elif [ $(($1 % 3)) -eq 0 ]; then
    echo "foo"    
elif [ $(($1 % 4)) -eq 0 ]; then
    echo "bar"
else
    echo "$1"
fi
```

### Boucle for

Modifiez `foobar` pour qu'il traite chaque nombre entre 1 et l'argument.

Exemple:

```
$ ./foobar 13
1
2
foo
bar
5
foo
7
bar
foo
10
11
foobar
13
$ foobar toto
foobar: toto n'est pas un entier
```

* Utilisez une boucle `for`
* Utilisez la commande `seq` pour générer une séquence de nombres entiers

```sh
#! /bin/sh

if echo "$1" | grep -vqE -e "^[0-9]+$" ; then
    echo "foobar: $1 n'est pas un entier" >&2
    exit 1
else
    for n in $(seq -s " " $1); do
        if [ $((n % 12)) -eq 0 ]; then
            echo "foobar"
        elif [ $((n % 3)) -eq 0 ]; then
            echo "foo"    
        elif [ $((n % 4)) -eq 0 ]; then
            echo "bar"
        else
            echo "$n"
        fi
    done
fi
```

### Boucle while (avancé)

Remplacez la boucle `for` (et le `seq`) par une boucle `while` et utilisez le développement arithmétique pour incrémenter une variable de boucle.

```sh
#! /bin/bash

if echo "$1" | grep -vqE -e "^[0-9]+$" ; then
    echo "foobar: $1 n'est pas un entier" >&2
    exit 1
else
    n=1
    while [ $n -le $1 ]; do
        if [ $((n % 12)) -eq 0 ]; then
            echo "foobar"
        elif [ $((n % 3)) -eq 0 ]; then
            echo "foo"    
        elif [ $((n % 4)) -eq 0 ]; then
            echo "bar"
        else
            echo "$n"
        fi
        n=$((n+1))
        # ou bien :  (( n++ ))
    done
fi
```
